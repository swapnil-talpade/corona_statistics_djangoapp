import requests
from django.shortcuts import render
from django.http import HttpResponse,Http404


# Create your views here.
def index(request):
	# https://api.covid19api.com/dayone/country/india/status/confirmed/live
	url='https://api.covid19api.com/live/country/{}/status/confirmed'
	url_total_cases='https://api.covid19api.com/dayone/country/{}/status/confirmed/live'

	country='india'

	if 'country' in request.GET:
		country=request.GET.get('country')

	try:
		response=requests.get(url.format(country)).json()
		response_total_cases=requests.get(url_total_cases.format(country)).json()

		country_data={
			'Country':response[-1]['Country'],
			'CountryCode':response[-1]['CountryCode'],
			'Total_Country_Cases':response_total_cases[-1]['Cases'],
			'Total_Country_Cases_Date':response_total_cases[-1]['Date'],
			'Confirmed':response[-1]['Confirmed'],
			'Deaths':response[-1]['Deaths'],
			'Recovered':response[-1]['Recovered'],
			'Active':response[-1]['Active'],
			'Date':response[-1]['Date'],
		}

		context={'country_data':country_data}
		return render(request,'corona_app/index.html',context)

	except IndexError:
		return render(request,"corona_app/error.html")

	



